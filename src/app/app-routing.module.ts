import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WelcomeComponent} from "./components/welcome/welcome.component";
import {QuestionsComponent} from "./components/questions/questions.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'Welcome', pathMatch: "full"
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'question',
    component: QuestionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
