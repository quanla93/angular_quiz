import { Component, OnInit } from '@angular/core';
import {QuestionService} from "../../service/question.service";
import {interval} from "rxjs";

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  public name : string ="";
  questionLists: any []= [];
  currentQuestion: number = 0;
  points: number = 0;
  counter: number = 60;
  correctAnswer: number = 0;
  inCorrectAnswer: number = 0;
  interval$: any;
  progress: string ="0";
  isQuizCompleted: boolean = false;

  constructor(private questionService: QuestionService) { }

  ngOnInit(): void {
    this.name = localStorage.getItem("name")!;
    this.getAllQuestion();
    this.startCounter();
  }

  getAllQuestion(){
    this.questionService.getQuestion().subscribe(data => {
      this.questionLists = data;
    });
  }

  nextQuestion() {
      this.currentQuestion++;
  }

  prevQuestion() {
    this.currentQuestion--;
  }

  answer(currentQues: number, option: any) {
    if(currentQues === this.questionLists.length){
        this.isQuizCompleted = true;
        this.stopCounter();
    }
    if (option.correct) {
      this.points += 10;
      this.correctAnswer++;
      setTimeout(() => {
        this.currentQuestion++;
        this.resetCounter()
        this.getProgressPercent();
      }, 800)
    } else {
      setTimeout(() => {
        this.currentQuestion++;
        this.inCorrectAnswer++;
        this.resetCounter();
        this.getProgressPercent();
      }, 800)
      this.points -= 10;
    }

  }

  startCounter(){
    this.interval$ = interval(1000)
      .subscribe(val => {
        this.counter--;
        if(this.counter === 0){
          this.currentQuestion++;
          this.counter=60;
          this.points-=10;
        }
      }); setTimeout(() => {
        this.interval$.unsubscribe();
    }, 600000)
  }
  stopCounter(){
    this.interval$.unsubscribe();
    this.counter=0;
  }
  resetCounter(){
    this.stopCounter();
    this.counter=60;
    this.startCounter();
  }

  resetQuiz(){
    this.resetCounter();
    this.getAllQuestion();
    this.points=0;
    this.counter=60;
    this.currentQuestion=0;
    this.progress="0";

  }
  getProgressPercent(){
    this.progress = ((this.currentQuestion/this.questionLists.length)*100).toString();
    return this.progress
  }
}
